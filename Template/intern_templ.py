from dal import DataAccess
import method_handlers
def posts_to_html(posts, full=True, auth=False, request=None):
    posts_html = ""

    for i in posts:
        posts_html += '<div class="post">'
        posts_html += '    <h2><a href="/posts/%s/">%s</a></h2>' % (i[0], i[1])
        len_text = 20
        if not full and len(i[2]) > len_text:
            posts_html += '    <p>%s...</p>' % (i[2][:20])
        else:
            posts_html += '    <p>%s</p>' % i[2]
        posts_html += '    <div class="date">Дата создания: %s</div>' % i[3]
        posts_html += '    <p class="like">'
        clas = ""
        if method_handlers.check_like_dislike_post(request, i[0], True):
            clas = "checked"
        posts_html += '        <span class = "%s">Like %s <a href="/posts/like/%s/"> <img src="/static/images/1like.png" alt="" width = "50" ></a></span>' % (
            clas, method_handlers.get_count_of_likes(i[0], True), i[0])
        clas = ""
        if method_handlers.check_like_dislike_post(request, i[0], False):
            clas = "checked"
        posts_html += '        <span class = "%s">Dislike %s <a href="/posts/dislike/%s/"><img src="/static/images/dislike.jpg" alt="" width = "50"></a></span>' % (
            clas, method_handlers.get_count_of_likes(i[0], False), i[0])
        posts_html += '    </p>'
        if auth:
            posts_html += '<a class="remove" href="/posts/delete/%s/">Удалить</a>' % i[0]
            posts_html += '    <div class="date">Дата редактирования: %s</div>' % i[4]
        posts_html += '</div>\n'
    return posts_html


def get_users_as_html(id=0):
    users = DataAccess.DataAccessor().select("select * from users where id <> %s " % id)
    users_html = ''
    for i in users:
        users_html += '<div class ="user" >'
        users_html += '    <p>Name: <a href="/users/%s/">%s</a></p>' % (i[0], i[1])
        users_html += '    <p> Last name: %s </p>' % i[2]
        users_html += '    <p> Username: %s </p>' % i[3]
        users_html += '</div>\n'
    return users_html