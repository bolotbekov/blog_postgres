import cgi#, cgitb
import logging
#from os import curdir
#from os import environ
from datetime import datetime
import uuid
import settings
from dal import DataAccess
#from template_engine.Template import Template
from http.server import HTTPStatus
from Template import template,intern_templ


def handle_index(request):
    user_id = is_authenticate(request)
    f = open(settings.TEMPLATES_DIR + 'index.html')
    read = f.read()
    posts = DataAccess.DataAccessor().select(
        "select id, title, post, createDate, editDate, user_id from posts where  datePublic <= '%s' ;" % (datetime.now()))
    posts_html =  intern_templ.posts_to_html(posts, full=False, auth=False, request=request)
    users_html = intern_templ.get_users_as_html()
    if user_id == -1:
        auth = False
    else:
        auth = True
    html = template.Template(read).render(auth = auth, users = users_html, posts_html = posts_html)
    request.send_response(HTTPStatus.OK)
    request.send_header('Content-Type', 'text/html')
    request.end_headers()
    request.wfile.write(str.encode(html))
    return request


def check_like_dislike_post(request, post_id, like):
    user_id = is_authenticate(request)
    likes = DataAccess.DataAccessor().select("select * from likes where user_id = %s and post_id = %s and liketype = %s" %(user_id, post_id, like))
    if len(likes)==0:
        return False
    else:
        return True


def like(request):
    """
    Вызввается при нажатии на кнопку лайк и дизлайк
    :param request:
    :return:
    """
    post_id = int(request.path.split('/')[-2])
    url_like = request.path.split('/')[-3] == 'like'
    user_id = is_authenticate(request)
    if user_id>0:
        like = DataAccess.DataAccessor().select("select id, liketype from likes where post_id = %s and user_id = %s" % (post_id,user_id))
        if len(like) == 0:
            DataAccess.DataAccessor().insert("likes", post_id = post_id, user_id = user_id, liketype = url_like)
        else:
            print("like type ",like[0][1], url_like,like[0][1]==url_like )
            if like[0][1] == url_like:
                DataAccess.DataAccessor().delete("likes", post_id = post_id, user_id = user_id)
            else:
                DataAccess.DataAccessor().update("UPDATE likes SET liketype = %s where id = %s" % (url_like,like[0][0]))
        redirect(request, str(get_prev_url(request)))
        request.end_headers()
    else:
        return handle_404(request,msg="Вы не авторизованы и не можете лайкнуть!!!")


def users(request):
    user_id = int(request.path.split('/')[-2])
    user = DataAccess.DataAccessor().select("select * from users where id = %s"%user_id)
    if len(user)>0:
        posts = DataAccess.DataAccessor().select("select id, title, post, createDate, editDate, user_id from posts where user_id = %s and datePublic <= '%s'" % (user_id, datetime.now()))
        posts_html = intern_templ.posts_to_html(posts = posts, request = request)
        read = open(settings.TEMPLATES_DIR + 'profile.html').read()
        users_html = intern_templ.get_users_as_html(user_id)
        html = template.Template(read).render( users_html = users_html, name = user[0][1], lname = user[0][2], username  = user[0][3], posts = posts_html)
        request.send_response(HTTPStatus.OK)
        request.send_header('Content-Type', 'text/html')
        request.end_headers()
        request.wfile.write(str.encode(html))
        return request
    else:
        return handle_404(msg="Пользователь с таким ID не существует!!!")


def post(request):
    post_id = int(request.path.split('/')[-2])
    user_id = is_authenticate(request)
    post = DataAccess.DataAccessor().select(
        "select id, title, post, createDate, editDate, user_id from posts where id = '%s'" % post_id)
    if len(post)>0:
        if user_id == int(post[0][5]):
            author = True
        else:
            author = False
        f = open(settings.TEMPLATES_DIR + 'post.html')
        read = f.read()
        html = template.Template(read).render( id = post_id, title = post[0][1], text = post[0][2], dateCreate = post[0][3], dateEdit = post[0][4] , author = author)
        request.send_response(HTTPStatus.OK)
        request.send_header('Content-Type', 'text/html')
        request.end_headers()
        request.wfile.write(str.encode(html))
    else:
        return handle_404(request, msg="Пост с таким Id не существует!!!ы")


def posts(request):
    """
    :param request:
    :return:
    """
    user_id = is_authenticate(request)
    if  user_id == -1:
        return handle_404(request, msg="Вы не авторизованы и не можете создавать посты!!!")
    else:
        data = get_value_from_post(request)
        DataAccess.DataAccessor().insert('posts',
                                         user_id = user_id,
                                         title = data['title'],
                                         post = data['text'],
                                         createDate = datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                         editDate = datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                         datePublic = str(data['date']) + ' ' + str(data['time'])
                                         )
        redirect(request, '/admin/')
        request.send_header('Content-Type', 'text/html')
        request.end_headers()
    return request


def remove_post(request):
    post_id = request.path.split('/')[-2]
    user_id = is_authenticate(request)
    post = DataAccess.DataAccessor().select("select user_id from posts where id = '%s'" % post_id)
    if len(post)>0:
        if int(post[0][0]) == user_id:
            DataAccess.DataAccessor().delete("posts", id = post_id)
            redirect(request, '/admin/')
            request.end_headers()
        else:
            f = open(settings.TEMPLATES_DIR + '404.html')
            read = f.read()
            html = template.Template(read).render(message = "Вы не являетесь автором поста с id = '%s'"%post_id)
            request.send_response(HTTPStatus.OK)
            request.send_header('Content-Type', 'text/html')
            request.end_headers()
            request.wfile.write(str.encode(html))


def edit_post(request):
    post_id = request.path.split('/')[-2]
    user_id = is_authenticate(request)
    post = DataAccess.DataAccessor().select("select * from posts where id = '%s'" % post_id)
    if len(post)>0:
        if int(post[0][1]) == user_id:
            request.send_response(HTTPStatus.OK)
            request.send_header('Content-Type', 'text/html')
            request.end_headers()
            f = open(settings.TEMPLATES_DIR + 'editpost.html')
            html = f.read()
            html = template.Template(html).render(id = post[0][0], title = post[0][2], text = post[0][3],date = str(post[0][6])[:10], time = str(post[0][6])[11:19])
            request.wfile.write(str.encode(html))
        else:
            return handle_404(request, msg="Вы не являетесть автором данного поста!!!")
    else:
        return handle_404(request, msg="Пост с таким id не существует!!!")
    return request

def post_update(request):
    post_id = request.path.split('/')[-2]
    user_id = is_authenticate(request)
    post = DataAccess.DataAccessor().select("select * from posts where id = '%s'" % post_id)
    if len(post) > 0:
        if int(post[0][1]) == user_id:
            newpost = get_value_from_post(request)
            for i in newpost:
                print (i, newpost[i]    )
            DataAccess.DataAccessor().update(
"""UPDATE posts SET
 title = '%s', post = '%s', editDate = '%s', datePublic = '%s' WHERE id = %s ;""" % (
                newpost['title'], newpost['text'], datetime.now().strftime("%Y-%m-%d %H:%M:%S"), str(newpost['date']) + ' ' + str(newpost['time']), post_id))
            redirect(request,'/posts/%s/'%post_id)
            request.end_headers()
            return request
        else:
            return handle_404(request, msg="Вы не являетесть автором данного поста!!!")
    else:
        return handle_404(request, msg="Пост с таким id не существует!!!")
    return request


def admin(request):
    user_id = is_authenticate(request)
    if user_id ==-1:
        read = open(settings.TEMPLATES_DIR + 'login.html').read()
        html = template.Template(read).render(msg = 'Enter login and password')
    else:
        read = open(settings.TEMPLATES_DIR + 'admin.html').read()
        data = get_userdata(request)
        posts = DataAccess.DataAccessor().select("select id, title, post, createDate, editDate, user_id from posts where user_id = '%s' ;" % (user_id))
        posts_html = intern_templ.posts_to_html(posts, full = False, auth=True, request = request)
        html = template.Template(read).render(
            name = data[0][1],
            lname = data[0][2],
            username  = data[0][3],
            posts = posts_html,
            date = datetime.now().date(),
            time = str(datetime.now().time())[:5]
        )
    request.send_response(HTTPStatus.OK)
    request.send_header('Content-Type', 'text/html')
    request.end_headers()
    request.wfile.write(str.encode(html))
    return request


def login_post(request):

    a = get_value_from_post(request)
    read = open(settings.TEMPLATES_DIR + 'login.html').read()
    user_id = check_user(a['username'],a['password'])
    if user_id ==-1:
        html = template.Template(read).render(
            msg="The password you've entered is incorrect"
        )
        request.send_response(HTTPStatus.OK)
        request.send_header('Content-Type', 'text/html')
        request.end_headers()
        request.wfile.write(str.encode(html))
    else:
        redirect(request,"/admin/")
        request.send_header('Set-Cookie', 'session=%s;path=/;' % authorize(a['username']))
        request.end_headers()
    return request


def register(request):
    read = open(settings.TEMPLATES_DIR + 'register.html').read()
    html = template.Template(read).render(
        msg='',
        name = '',
        lname = ''
    )
    if is_authenticate(request) != -1:
        redirect(request, '/')
    else:
        request.send_response(HTTPStatus.OK)
        request.send_header('Content-Type', 'text/html')
    request.end_headers()
    request.wfile.write(str.encode(html))
    return request


def reg_post(request):
    values = get_value_from_post(request)
    a = DataAccess.DataAccessor()
    if a.is_exist_user(values['username']):
        return handle_404(request,msg="Such user already exist!!!")
    else:
        redirect(request, "/admin/")
        a.insert('users',
            name = values['name'],
            lname = values['lname'],
            username = str(values['username']).lower(),
            password = values['password'],
            active = True
        )
        session = str(uuid.uuid4())
        request.send_header('Set-Cookie', 'session=%s;path=/;' % session)
        a.insert( 'sessions',
            id_user = str(get_id_by_username(values['username'])).lower(),
            session = session
        )
        request.end_headers()
    return request


def logout(request):
    if is_authenticate(request)!=-1:
        cook = get_cookies(request)
        if "session" in cook:
            remove_session(cook['session'])
    request.send_response(HTTPStatus.TEMPORARY_REDIRECT)
    request.send_header('Set-Cookie', 'session=;path=/;')
    request.send_header('Location', '/')
    request.end_headers()
    return request


def handle_404(request, msg = ""):
    request.send_response(HTTPStatus.NOT_FOUND)
    request.send_header('Content-Type', 'text/html')
    request.end_headers()
    f = open(settings.TEMPLATES_DIR + '404.html')
    html = f.read()
    if msg=="":
        msg = "Такая страница не существует!!!"
    html = template.Template(html).render(message = msg)
    request.wfile.write(str.encode(html))
    return request


def return404(request, msg):
    request.send_response(HTTPStatus.NOT_FOUND)
    request.send_header('Content-Type', 'text/html')
    request.end_headers()
    read = open(settings.TEMPLATES_DIR + '404.html').read()
    html = template(read).render(message = msg)
    request.wfile.write(str.encode(html))
    return request


def resetDB(request):
    a = DataAccess.DataAccessor()
    a.creteable()
    print("Data base updated")
    request.send_header('Content-Type', 'text/html')
    request.end_headers()
    request.wfile.write(str.encode('Data base updated'))
    return request


def get_value_from_post(request):
    res = {}
    #logging.debug('POST %s' % (request.path))
    ctype, pdict = cgi.parse_header(request.headers['content-type'])
    if ctype == 'multipart/form-data':
        pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
        for i in pdict:
            print("pdict = ",i,"   $      " ,pdict[i])
        print("rquest rfile", request.rfile)
        postvars = cgi.parse_multipart(request.rfile, pdict)
        for key in sorted(postvars):
            #print("i = %s,     key[i] = %s" % (key, postvars[key][0].decode("utf-8")))
            #logging.debug('ARG[%d] %s=%s' % (i, key, postvars[key]))
            a = key#.decode("utf-8")
            if key != 'picture':
                b = postvars[key][0].decode("utf-8")
            else:
                import mimetypes
                b = postvars[key][0]
                mime = mimetypes.guess_type(settings.STATIC_DIR+'/images/lisdsfke.jpg',strict=b)
                print("mime =",mime)
                f = open(settings.STATIC_DIR+'/images/asd', 'wb')
                f.write( postvars[key][0])
                f.close()
            # print('ARG[%d] %s=%s' % (i, key, postvars[key]))
            res[a] = b
    elif ctype == 'application/x-www-form-urlencoded':
        length = int(request.headers['content-length'])
        postvars = cgi.parse_qs(request.rfile.read(length), keep_blank_values=1)
        for key in sorted(postvars):
            #logging.debug('ARG[%d] %s=%s' % (i, key, postvars[key]))
            a = key.decode("utf-8")
            b = postvars[key][0].decode("utf-8")
            res[a] = b
    else:
        postvars = {}
    logging.debug('TYPE %s' % (ctype))
    logging.debug('PATH %s' % (request.path))
    logging.debug('ARGS %d' % (len(postvars)))
    return res


def is_authenticate(request):
    cook = get_cookies(request)
    if 'session' in cook:
        a = DataAccess.DataAccessor()
        rows = a.select("select id_user from sessions where session='%s';" % cook['session'])
        if len(rows)!=0:
            return rows[0][0]
        else:
            return -1
    else:
        return -1


def get_cookies(request):
    """
    return cookies from request
    :param request:
    :return:
    """
    res = {}
    cookie = (request.headers.get_all('Cookie', failobj={}))
    if len(cookie)>0:
        for i in cookie[0].split(';'):
            res[(i.strip().split('='))[0]]= i.strip().split('=')[1]
    return res


def get_prev_url(request):
    return str(request.headers.get_all('Referer'))[2:-2]


def get_id_by_username(username):
    #query = "select id from users order by id desc limit 1;"
    query = "select id from users where username='%s';"%username
    a = DataAccess.DataAccessor()
    rows = a.select(query)
    if len(rows) > 0:
        return rows[0][0]
    else:
        return -1


def check_user(username, password):
    """
    :param username:
    :param password:
    :return: if exist return user_id else -1
    """
    a = DataAccess.DataAccessor()
    rows = a.select("select * from users where username='%s' and password='%s';" % (username, password))
    if len(rows)>0:
        return rows[0][0]
    else:
        return -1


def redirect(request, path):
    print("redirect path =!%s!" % path)
    request.send_response(HTTPStatus.FOUND)
    request.send_header('Location', path)


def remove_session(session):
    a = DataAccess.DataAccessor()
    a.delete("sessions", session = session)


def authorize(username):
    """
    generate and return session
    :param username:
    :return:
    """
    a = DataAccess.DataAccessor()
    session = str(uuid.uuid4())
    a.insert("sessions", id_user = get_id_by_username(username), session = session)
    return session


def get_userdata(request):
    """
    return current users data
    :param request:
    :return:
    """
    a = DataAccess.DataAccessor()
    return a.select("select * from users where id = '%s'" % is_authenticate(request))


def return_static(request):
    #path = request.path

    try:
        sendReply = False
        if request.path.endswith(".jpg"):
            mimetype = 'image/jpg'
            sendReply = True
        if request.path.endswith(".gif"):
            mimetype = 'image/gif'
            sendReply = True
        if request.path.endswith(".png"):
            mimetype = 'image/png'
            sendReply = True
        if request.path.endswith(".js"):
            mimetype = 'application/javascript'
            sendReply = True
        if request.path.endswith(".css"):
            mimetype = 'text/css'
            sendReply = True
        if sendReply:
            # Open the static file requested and send it
            file_path = (settings.STATIC_DIR[:-7] + request.path)
            request.send_response(200)
            request.send_header('Content-type', mimetype)
            if request.path.endswith('.jpg')or\
                    request.path.endswith('.jpeg') or\
                    request.path.endswith('.gif') or \
                    request.path.endswith('.png'):
                def load_benary(file):
                    with open(file, 'rb') as file:
                        return file.read()
                read = load_benary(file_path)
                request.wfile.write(bytes(read))
            else:
                f = open(file_path)
                read = f.read()
                request.end_headers()
                request.wfile.write(bytes(read, 'utf-8'))
                f.close()
            return
    except IOError:
        request.send_error(404, 'File Not Found: %s' % request.path)


def get_count_of_likes(posts_id, i):
    return len(DataAccess.DataAccessor().select("select id from likes where post_id = %s and liketype = %s" % (posts_id, i)))