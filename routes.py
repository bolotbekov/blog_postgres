from routing import Route
from constants import HTTP_METHODS
import method_handlers

routes = [
    Route(HTTP_METHODS.GET, '/$', method_handlers.handle_index),
    Route(HTTP_METHODS.GET, '/resetdb/', method_handlers.resetDB),
    Route(HTTP_METHODS.GET, '/register/', method_handlers.register),
    Route(HTTP_METHODS.POST, '/register/', method_handlers.reg_post),
    Route(HTTP_METHODS.GET, '/logout/', method_handlers.logout),
    #Route(HTTP_METHODS.GET, '/login/', method_handlers.login),
    Route(HTTP_METHODS.GET, '/admin/', method_handlers.admin),
    Route(HTTP_METHODS.POST, '/admin/', method_handlers.login_post),
    Route(HTTP_METHODS.GET, '/posts/\d+/', method_handlers.post),
    Route(HTTP_METHODS.GET, '/posts/like/\d+/', method_handlers.like),
    Route(HTTP_METHODS.GET, '/posts/dislike/\d+/', method_handlers.like),
    Route(HTTP_METHODS.GET, '/posts/delete/\d+/', method_handlers.remove_post),
    Route(HTTP_METHODS.GET, '/posts/edit/\d+/', method_handlers.edit_post),
    Route(HTTP_METHODS.POST, '/posts/edit/\d+/', method_handlers.post_update),
    Route(HTTP_METHODS.POST, '/post/', method_handlers.posts),
    Route(HTTP_METHODS.GET, '/static/', method_handlers.return_static),
    Route(HTTP_METHODS.GET, '/users/\d+/', method_handlers.users),



]















































Route(HTTP_METHODS.GET, '/posts/edit/\d+/', method_handlers.edit_post),